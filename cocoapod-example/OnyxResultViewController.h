//
//  OnyxResultViewController.h
//  cocoapod-example
//
//  Created by Matthew Wheatley on 10/15/18.
//  Copyright © 2018 Diamond Fortress Technologies, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "iCarousel.h"
#import <MessageUI/MessageUI.h>

@interface OnyxResultViewController : UIViewController <iCarouselDataSource, iCarouselDelegate, MFMailComposeViewControllerDelegate>

#if !TARGET_IPHONE_SIMULATOR
@property OnyxResult *onyxResult;
#endif
@property (nonatomic, weak) IBOutlet UIStackView *livenessStackView;
@property (nonatomic, weak) IBOutlet UITextView *livenessTextView;
@property (nonatomic, weak) IBOutlet UIStackView *processedStackView;
@property (nonatomic, weak) IBOutlet iCarousel *processedCarousel;
@property (nonatomic, weak) IBOutlet UIStackView *rawStackView;
@property (nonatomic, weak) IBOutlet iCarousel *rawCarousel;

#if !TARGET_IPHONE_SIMULATOR
- (IBAction)save:(id)sender;
#endif

@end
