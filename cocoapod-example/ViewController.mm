//
//  ViewController.m
//  cocoapod-example
//
//  Created by Matthew Wheatley on 10/15/18.
//  Copyright © 2018 Diamond Fortress Technologies, Inc. All rights reserved.
//

#import "ViewController.h"
#if !TARGET_IPHONE_SIMULATOR
#import "OnyxResultViewController.h"
#endif

@interface ViewController ()

@end

@implementation ViewController

UITextField* activeTextField;
CGPoint lastOffset;
float keyboardHeight;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    _backgroundColorHexString.delegate = self;
    _cropFactor.delegate = self;
    _cropSizeWidth.delegate = self;
    _cropSizeHeight.delegate = self;
    _backButtonText.delegate = self;
    _manualCaptureText.delegate = self;
    _infoText.delegate = self;
    _infoTextColorHexString.delegate = self;
    _base64ImageData.delegate = self;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"segueToOnyxResult"]) {
#if !TARGET_IPHONE_SIMULATOR
        OnyxResultViewController* orvc = segue.destinationViewController;
        orvc.onyxResult = sender;
#endif
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Actions
- (IBAction)capture:(UIButton *)sender {
    
#if !TARGET_IPHONE_SIMULATOR
    OnyxConfigurationBuilder* onyxConfigBuilder = [[OnyxConfigurationBuilder alloc] init];
    onyxConfigBuilder
            .licenseKey(@"your license key here")
            .showLoadingSpinner(YES)
            .returnRawImage(_returnRawImage.on)
            .returnProcessedImage(_returnProcessedImage.on)
            .returnWSQ(_returnWsq.on)
            .useOnyxLive(_useOnyxLive.on)
            .returnFingerprintTemplate(INNOVATRICS)
            .reticleOrientation((ReticleOrientation) _reticleOrientation.selectedSegmentIndex)
            .computeNfiqMetrics(true)
            .showLoadingSpinner(YES)
            .returnSlapImage(NO)
            .shouldBinarizeProcessedImage(NO)
            .returnFullFrameImage(NO)
            .useManualCapture(NO)
            .manualCaptureText(@"Tap to manual capture")
            .captureFingersText(@"Hold fingers steady")
            .captureThumbText(@"Hold thumb steady")
            .fingersNotInFocusText(@"Move fingers until in focus")
            .thumbNotInFocusText(@"Move thumb until in focus")
            .uploadMetrics(YES)
            .returnOnyxErrorOnLowQuality(YES)
            .captureQualityThreshold(0.7)
            .fingerDetectionTimeout(60)
           // .triggerCaptureOnFingerDetectTimeout(false)
            .successCallback([self onyxSuccessCallback])
            .errorCallback([self onyxErrorCallback])
            .onyxCallback([self onyxCallback]);
    
    [onyxConfigBuilder buildOnyxConfiguration];
#endif
}

#pragma mark - Onyx Callbacks

#if !TARGET_IPHONE_SIMULATOR
-(void(^)(Onyx* configuredOnyx))onyxCallback {
    return ^(Onyx* configuredOnyx) {
        NSLog(@"Onyx Callback");
        dispatch_async(dispatch_get_main_queue(), ^{
            [configuredOnyx capture:self];
        });

    };
}

-(void(^)(OnyxResult* onyxResult))onyxSuccessCallback {
    return ^(OnyxResult* onyxResult) {
        NSLog(@"Onyx Success Callback");
        self->_onyxResult = onyxResult;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            [self performSegueWithIdentifier:@"segueToOnyxResult" sender:onyxResult];
        }];
    };
}

-(void(^)(OnyxError* onyxError)) onyxErrorCallback {
    return ^(OnyxError* onyxError) {
        NSLog(@"Onyx Error Callback");
        dispatch_async(dispatch_get_main_queue(), ^{
            //            [self stopSpinnner];
            UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"ONYX Error"
                                          message:[NSString stringWithFormat:@"ErrorCode: %d, ErrorMessage:%@, Error:%@", onyxError.error, onyxError.errorMessage, onyxError.exception]
                                          preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *okAction = [UIAlertAction
                        actionWithTitle:@"OK"
                                  style:UIAlertActionStyleDefault
                                handler:nil];

            [alertController addAction:okAction];

            [self presentViewController:alertController animated:YES completion:nil];
        });
            
    };
}
#endif

#pragma mark - TextFields

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    lastOffset = _scrollView.contentOffset;
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    activeTextField = nil;
    return true;
}

#pragma mark - keyboard movements

- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

@end
